﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NadekoBot.Migrations
{
    public partial class okerrorcolors : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ErrorColor",
                table: "BotConfig",
                nullable: false,
                defaultValue: "ee281f");

            migrationBuilder.AddColumn<string>(
                name: "OkColor",
                table: "BotConfig",
                nullable: false,
                defaultValue: "71cd40");
				
			migrationBuilder.AddColumn<string>(
                name: "LogMinorColor",
                table: "BotConfig",
                nullable: false,
                defaultValue: "ff69b4");
				
			migrationBuilder.AddColumn<string>(
                name: "LogMediumColor",
                table: "BotConfig",
                nullable: false,
                defaultValue: "71cd40");	

			migrationBuilder.AddColumn<string>(
                name: "LogUrgentColor",
                table: "BotConfig",
                nullable: false,
                defaultValue: "ee281f");					
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ErrorColor",
                table: "BotConfig");

            migrationBuilder.DropColumn(
                name: "OkColor",
                table: "BotConfig");
				
            migrationBuilder.DropColumn(
                name: "LogMinorColor",
                table: "BotConfig");

            migrationBuilder.DropColumn(
                name: "LogMediumColorColor",
                table: "BotConfig");

            migrationBuilder.DropColumn(
                name: "LogUrgentColor",
                table: "BotConfig");				
        }
    }
}
