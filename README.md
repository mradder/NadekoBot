[![Build status](https://ci.appveyor.com/api/projects/status/qtdv521l8ytdtreq?svg=true)](https://ci.appveyor.com/project/MrAdder/nadekobot-enkl9)

[![Documentation Status](https://readthedocs.org/projects/nadekobot/badge/?version=latest)](http://nadekobot.readthedocs.io/en/latest/?badge=latest)


## For Updates, Help and Guidelines

| [![twitter](https://cdn.discordapp.com/attachments/155726317222887425/252192520094613504/twiter_banner.JPG)](https://twitter.com/MrAdder) | [![discord](https://cdn.discordapp.com/attachments/266240393639755778/281920766490968064/discord.png)](https://discord.gg/6WSCZSz)
| --- | --- | --- |
| **Follow me on Twitter.** | **Join my Discord server for help.** |
